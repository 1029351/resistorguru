﻿(function () {
    "use strict";

    WinJS.UI.Pages.define("/pages/home/home.html", {
        // This function is called whenever a user navigates to this page. It
        // populates the page elements with the app's data.
        ready: function (element, options) {
            // TODO: Initialize the page here.
            function zip(xs, ys) {
                var zs = [];
                for (var i = 0; i < xs.length; ++i) {
                    zs.push([xs[i], ys[i]]);
                }
                return zs;
            }

            function sel(s, v) {
                for (var i, j = 0; i = s.options[j]; ++j) {
                    if (i.value === v) {
                        s.selectedIndex = j;
                        break;
                    }
                }
            }

            var cfcolor = '#FADF8E';
            var mfcolor = '#99D6E8';

            var colors = ['black', 'brown', 'red', 'orange', 'yellow', 'green',
                          'blue', 'violet', 'grey', 'white', 'gold', 'silver'];

            var table = {};
            for (var i = 0; i < colors.length; ++i) {
                table[colors[i]] = {
                    'first': i,
                    'second': i,
                    'third': i,
                    'multiplier': i
                };
            }

            [table.gold, table.silver].forEach(function (color) {
                color.first = color.second = color.third = null;
            });

            function assignTableProperties(property, keys, values) {
                zip(keys, values).forEach(function (p) {
                    table[p[0]][property] = p[1];
                });
            }

            assignTableProperties('multiplier',
                                  ['grey', 'white', 'gold', 'silver'],
                                  [null, null, -1, -2]);

            assignTableProperties('tolerance',
                ['brown', 'red', 'green', 'blue', 'violet', 'grey', 'gold', 'silver'],
                [1, 2, 0.5, 0.25, 0.10, 0.05, 5, 10]);

            var cfradio = document.getElementById('cfradio');
            var mfradio = document.getElementById('mfradio');

            var value = document.getElementById('value');
            var resistor = {};
            var inputs = ['first', 'second', 'third',
                         'multiplier', 'tolerance'].map(function (id) {
                             var select = document.getElementById(id);
                             var f = function (e) {
                                 if (e.target.id) {
                                     resistor[e.target.id].style.backgroundColor = e.target.value;
                                 }
                             };
                             var g = function (e) {
                                 value.value = decodeResistor.apply(this, inputs.map(function (element) {
                                     return element.value;
                                 })).value;
                             };
                             resistor[id] = document.getElementById(id + 'band');
                             resistor[id].style.backgroundColor = select.value;
                             select.addEventListener('change', f);
                             select.addEventListener('click', f);
                             select.addEventListener('change', g);
                             select.addEventListener('click', g);
                             return select;
                         });

            function decodeResistor(first, second, third, multiplier, tolerance) {
                return {
                    'value': +('' + table[first].first
                                  + table[second].second
                                  + table[third].third)
                             * Math.pow(10, table[multiplier].multiplier),
                    'tolerance': table[tolerance].tolerance
                };
            }

            function encodeResistor(value) {
                var p = getColor() === cfcolor;
                var multiplier;
                if (value === 0) {
                    multiplier = -2;
                } else {
                    multiplier = (Math.log(value) / Math.log(10)) | 0;
                }
                if (value >= 1) {
                    multiplier -= p ? 1 : 2;
                } else {
                    --multiplier;
                }
                if (value !== 0 && (value === 1e3)) {
                    ++multiplier;
                }
                if (value !== 0 && (value === 1e6)) {
                    ++multiplier;
                }
                if (value >= 1e9) {
                    return null;
                }
                if (0.1 <= value && value < 1) {
                    --multiplier;
                }
                value *= 100;
                if (0 < value && value < 1) {
                    return null;
                }
                value += '';
                var first = value[0];
                var second = value[1];
                var third = p ? null : value[2];
                return {
                    'first': first,
                    'second': second,
                    'third': third,
                    'multiplier': multiplier
                };
            }

            function getColor() {
                return cfradio.checked ? cfcolor : mfcolor;
            }

            function applyColor(c) {
                var b1 = document.getElementsByClassName('medband');
                var b2 = document.getElementsByClassName('smallband');
                for (var i = 0; i < b1.length; ++i) {
                    b1[i].style.backgroundColor = c;
                }
                for (var i = 0; i < b2.length; ++i) {
                    b2[i].style.backgroundColor = c;
                }
                resistor.tolerance.style.backgroundColor = c === cfcolor ? 'gold' : 'brown';
                sel(inputs[4], c === cfcolor ? 'gold' : 'brown');
                inputs[2].hidden = c === cfcolor;
                applyResistorValue();
            }

            cfradio.addEventListener('click', applyColor.bind(this, cfcolor));
            mfradio.addEventListener('click', applyColor.bind(this, mfcolor));

            applyColor(cfcolor);

            function applyResistorValue() {
                var c = getColor();
                var v = +value.value;
                if ((!v && v !== 0) || v < 0) {
                    return;
                }
                var r = encodeResistor(v);
                if (!r) {
                    return;
                }
                var f = resistor.first.style.backgroundColor = colors[r.first] || c;
                var s = resistor.second.style.backgroundColor = colors[r.second] || c;
                var t = resistor.third.style.backgroundColor = colors[r.third] || c;
                var m = resistor.multiplier.style.backgroundColor = colors[r.multiplier] || ({
                    '-1': 'gold',
                    '-2': 'silver'
                }[r.multiplier]) || c;
                sel(inputs[0], f);
                sel(inputs[1], s);
                sel(inputs[2], t);
                sel(inputs[3], m);
            }

            value.addEventListener('keyup', applyResistorValue);    
            applyResistorValue();

            /* var options = document.getElementsByTagName('option');
             for (var i = 0; i < options.length; ++i) {
               options[i].style.color = options[i].value;
               options[i].style.borderColor = options[i].value;
               options[i].style.backGroundColor = options[i].value;
             }*/
        }
    });
})();
