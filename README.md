Resistor Guru
=============

Resistor Guru is an app that decodes resistor color codes into resistance
values and encodes resistance values into its equivalent color code. It also
features a table of resistor color bands. Resistor Guru supports 4-band
Carbon Film resistors and 5-band Metal Film resistors.

Resistor Guru is free software under the MIT license.

Resistor Guru is implemented as a HTML5 and Javascript application targeting Windows 8.